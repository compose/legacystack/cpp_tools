
set(MODULE_NAME colors)


add_library(${MODULE_NAME} INTERFACE)
add_library(cpp_tools::${MODULE_NAME} ALIAS ${MODULE_NAME})
target_compile_features(${MODULE_NAME} INTERFACE cxx_std_17)
list(APPEND CPP_TOOLS_TARGETS ${MODULE_NAME})

# Set library includes
# --------------------
target_include_directories(${MODULE_NAME} INTERFACE
    $<BUILD_INTERFACE:${CPP_TOOLS_ROOT}/${MODULE_NAME}/include/>
    $<BUILD_INTERFACE:${CPP_TOOLS_ROOT}/${MODULE_NAME}/include/>
  $<INSTALL_INTERFACE:include>
  )
